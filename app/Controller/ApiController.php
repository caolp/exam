<?php

/**
 * Created by IntelliJ IDEA.
 * User: caolp
 * Date: 4/23/2016
 * Time: 8:22 PM
 */
class ApiController extends AppController
{
    public $uses = array(
        'Question'
    );
    public $component = array('RequestHandler');

    public function questions($room_id = null)
    {
        $this->layout = 'json';
        if ($room_id) {
            $questions = Cache::read($room_id, 'game');
            if (!$questions) {
                $questions = $this->Question->find('all', array(
                    'fields' => 'Question.id, Question.point',
                    'order' => 'RAND()',
                    'limit' => 36,
                    'recursive' => -1
                ));
                $row = 'A';
                $column = 1;
                foreach ($questions as $i => $question) {
                    $questions[$i]['Question']['attr_id'] =  $row . $column;
                    $questions[$i]['Question']['data_id'] = $i;
                    $column++;
                    if ((($i + 1) % 6) == 0) {
                        $column = 1;
                        $row++;
                    }
                }
                Cache::write($room_id, $questions, 'game');
            }
            $this->set(array(
                'questions' => $questions,
                '_serialize' => array('questions')
            ));
        } else {

        }

    }

    public function question_by_id($id)
    {
        $this->layout = 'json';
        $question = $this->Question->findById($id);
        $this->set(array(
            'question' => $question,
            '_serialize' => array('question')
        ));
    }
}