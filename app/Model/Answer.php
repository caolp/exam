<?php
/**
 * Created by IntelliJ IDEA.
 * User: CaoLP
 * Date: 4/20/2016
 * Time: 3:35 PM
 */
class Answer extends AppModel{
	public $belongsTo = array(
		'Question' => array(
			'className' => 'Question',
			'foreignKey' => 'question_id'
		)
	);

}
