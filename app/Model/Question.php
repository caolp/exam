<?php
/**
 * Created by IntelliJ IDEA.
 * User: CaoLP
 * Date: 4/20/2016
 * Time: 3:34 PM
 */
class Question extends AppModel{
	public $hasMany = array(
		'Answer' => array(
			'className' => 'Answer',
			'foreignKey' => 'question_id',
			'order' => 'RAND()'
		)
	);
}
