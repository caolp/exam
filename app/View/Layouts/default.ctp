<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
	echo $this->Html->meta('icon');

	echo $this->Html->css('cake.generic');

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->Html->script(array(
		'//code.jquery.com/jquery-2.2.1.min.js',
	));
	echo $this->fetch('script');
	?>
</head>
<body>
<div id="container">
	<div id="header">
		<?php echo $this->Html->link('Home', '/');?>
	</div>
	<div id="content">

		<?php echo $this->Flash->render(); ?>

		<?php echo $this->fetch('content'); ?>
	</div>
	<div id="footer">
	</div>
</div>
</body>
</html>
