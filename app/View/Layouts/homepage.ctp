<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-us">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="maximum-scale=1.0,initial-scale=1.0"/>
    <title>Kyanon Game</title>
    <?php
    echo $this->Html->css(array(
        'layout',
        'anim',
        '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css'
    ));
    echo $this->Html->script(array(
        '//code.jquery.com/jquery-1.10.2.js',
        '//code.jquery.com/ui/1.11.4/jquery-ui.js'
    ));
    ?>
</head>

<body>
<script id="dialog_content" type="text/html">
    {{#Question}}
    <div title="Question with {{point}} point" id="dia">
        <div class="body">
            <h2>{{content}}</h2>
            {{/Question}}
            <ul class="question-list">
                {{#Answer}}
                <li>
                    <a class="btn" href="javascript:;" onclick="showRes(this)"
                       data-correct="{{is_correct}}">{{content}}</a>
                </li>
                {{/Answer}}
            </ul>
        </div>
    </div>
</script>
<div class="dont-click"></div>
<section id="pc" class="page">
    <?php echo $this->fetch('content') ?>
</section>
<section id="mobile">
    <div>
        <h4>Questions</h4><a href="javascript:;" id="closebox">X</a>
        <input name="selected" id="selected">
        <button class="sendbtn">Select</button>
        <hr>
        <h4>Answers</h4>
        <button type="button" class="ans" data-id="0">A</button>
        <button type="button" class="ans" data-id="1">B</button>
        <button type="button" class="ans" data-id="2">C</button>
        <button type="button" class="ans" data-id="3">D</button>
    </div>
</section>

<?php
echo $this->Html->script(
    array(
        'socket.io-1.4.5',
        'mustache.min',
        'gamelogic'
    )
);
?>
</body>
</html>
