<section class="gameboard">
    <h2 id="message">Join me in #id</h2>
    <script>
        var questions = <?php echo json_encode($questions)?>;
    </script>
    <ul>
        <?php
        $row = 'A';
        $column = 1;
        foreach ($questions as $i => $question) {
            ?>
            <li id="card-<?php echo $row . $column ?>" class="card card-6" data-id="<?php echo $i ?>">
                <span></span>
				<span class="back">
					<?php echo $question['Question']['point'] ?>
				</span>
            </li>
            <?php
            $column++;
            if ((($i + 1) % 6) == 0) {
                $column = 1;
                $row++;
            }
        }
        ?>

    </ul>

    <p class="win">You won! Woohoo!</p>
</section>
<script>

</script>
