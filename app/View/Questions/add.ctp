<style>
	.input-answer {
		height: 40px;
	}
</style>
<script>
$(function () {
	$('.answer input[type="checkbox"]').on('change', function () {
		if($(this).is(':checked')){
			$('.answer input[type="checkbox"]').prop('checked', false);
			$(this).prop('checked', true);
		}
	});
})
</script>
<div class="questions form">
	<?php echo $this->Form->create('Question'); ?>
	<fieldset>
		<legend><?php echo __('Add Question'); ?></legend>
		<?php
		echo $this->Form->input('content');
		echo $this->Form->input('point');
		?>
	</fieldset>
	<fieldset>
		<legend><?php echo __('Add Answer'); ?></legend>
	</fieldset>
	<fieldset class="answer">
		<legend><?php echo __('#1'); ?></legend>
		<?php
		echo $this->Form->input('Answer.0.content', array('class' => 'input-answer'));
		echo $this->Form->input('Answer.0.is_correct', array('type' => 'checkbox'));
		?>
	</fieldset>
	<fieldset class="answer">
		<legend><?php echo __('#2'); ?></legend>
		<?php
		echo $this->Form->input('Answer.1.content', array('class' => 'input-answer'));
		echo $this->Form->input('Answer.1.is_correct', array('type' => 'checkbox'));
		?>
	</fieldset>
	<fieldset class="answer">
		<legend><?php echo __('#3'); ?></legend>
		<?php
		echo $this->Form->input('Answer.2.content', array('class' => 'input-answer'));
		echo $this->Form->input('Answer.2.is_correct', array('type' => 'checkbox'));
		?>
	</fieldset>
	<fieldset class="answer">
		<legend><?php echo __('#4'); ?></legend>
		<?php
		echo $this->Form->input('Answer.3.content', array('class' => 'input-answer'));
		echo $this->Form->input('Answer.3.is_correct', array('type' => 'checkbox'));
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('List Questions'), array('action' => 'index')); ?></li>
	</ul>
</div>
