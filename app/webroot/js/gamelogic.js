if (window.top !== window.self) window.top.location.replace(window.self.location.href);

var animating;
var server = 'http://wip.kyanon.com:3003';
var socket = io.connect(server);
// If client is an Android Phone
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $('#mobile').show();
    var gameCode = window.location.hash.substr(1, 6);
    $(function () {
        // When server replies with initial welcome...
        socket.on('welcome', function (data) {
            // Send 'controller' device type with our entered game code
            socket.emit("device", {"type": "controller", "gameCode": gameCode});
        });
        socket.on("connected", function (data) {
            console.log('connected');
            $('.sendbtn').on('click', function (e) {
                var id = $('#selected').val();
                socket.emit("question", {'question': id});
            });
            $('.ans').on('click', function (e) {
                var answer = $(this).data('id');
                socket.emit("answer", {'answer': answer});
            });
            $('#closebox').on('click', function (e) {
                socket.emit("closebox", {'closebox': true});
            });
        });
    });

    function sendBtn(answer) {
        socket.emit("answer", {'answer': answer});
    }

    function sendQuestion() {
        var id = $('#selected').val();
        socket.emit("question", {'question': id});
    }
} else {
    var gameCode = window.location.hash.substr(1, 6);
    $('#pc').show();
    $(document).ready(
        function () {
            if (gameCode) {
                $('.dont-click').show();
                // When server replies with initial welcome...
                socket.on('welcome', function (data) {
                    socket.emit("device", {"type": "game", "gameCode": gameCode});
                });
                socket.on("connected", function (data) {
                    $('#message').html('Let\' play!');
                    console.log("connected");
                    socket.on("answer", function (answer) {
                        console.log(answer);
                        pressBtn(answer);
                    });
                    socket.on("question", function (question) {
                        console.log(question);
                        selectQuestionByid(question);
                    });
                    socket.on("closebox", function (closebox) {
                        $('#dia').dialog('close');
                    });
                    $(".card").bind('click', selectCard);
                });
            } else {
                // When server replies with initial welcome...
                socket.on('welcome', function (data) {
                    socket.emit("device", {"type": "game"});
                });
                // We receive our game code to show the user
                socket.on("initialize", function (gameCode) {
                    $('#message').html('Join me in #' + gameCode);
                });
                socket.on("answer", function (answer) {
                    console.log(answer);
                    pressBtn(answer);
                });
                socket.on("question", function (question) {
                    console.log(question);
                    selectQuestionByid(question);
                });
                socket.on("closebox", function (closebox) {
                    $('#dia').dialog('close');
                });
                $(".card").bind('click', selectCard);
            }
        }
    );

}


function selectCard() {
    $(".card:not(.card-selected)").removeClass('card-selected');

    if (!animating) {
        if (!$(this).hasClass('card-selected')) {
            var index = $(this).data('id');
            setTimeout(showDialog(index), 100)
        }
        $(this).addClass('card-selected');
    }
}

function showDialog(index) {
    animating = true;
    console.log(questions[index]);
    var view = questions[index];
    var template = document.getElementById('dialog_content').innerHTML;
    var output = Mustache.render(template, view);
    $(output).dialog(
        {
            width: 1024,
            height: 500,
            show: {
                effect: "scale",
                duration: 500
            },
            hide: {
                effect: "drop",
                duration: 200
            },
            close: function (event, ui) {
                $(this).dialog('destroy').remove();
                animating = false;
            }
        }
    );
    var anpha = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'];
    var i = 0;
    $('.btn').each(function (index, value) {
        $(this).prepend(anpha[i] + '. ');
        $(this).addClass('btn-' + i);
        i++;
    })
}
var total = 0;
function showRes(element) {
    if ($(element).hasClass('disabled')) return;
    if ($(element).data('correct') == 1) {
        $(element).addClass('correct');
        total = 2;
    } else {
        total++;
        $(element).addClass('failed');
    }
    $(this).addClass('disabled');
    if (total >= 2) {
        $('.btn').each(function (index, value) {
            if ($(this).data('correct') == 1) {
                $(this).addClass('correct');
            }
            $(this).addClass('disabled');
        });
        total = 0;
    }


}

function pressBtn(btn) {
    $('.btn-' + btn).click();
}
function selectQuestionByid(id) {
    $('#card-' + id.toUpperCase()).click();
}
function selectQuestion() {
    var id = $('#selected').val();
    $('#card-' + id.toUpperCase()).click();
}
function checkCards() {

    var found = 0;

    for (i = 1; i <= 12; i++) {
        if (found > 0)
            continue;

        if ($(".card-selected.card-" + i).length > 2) {
            found = i;
            continue;
        } else {
        }
    }

    if (found > 0)
        foundCards(found);

    animating = true;
    setTimeout(function () {
        $(".card").removeClass('card-selected');
        animating = false;
    }, 750);
}

function foundCards(type) {
    $(".card-" + type).delay(250).animate(
        {
            opacity: 0
        }, 500,
        function () {
            $(this).addClass("card-found");

            if ($(".card:not(.card-found)").length == 0) {
                $("body").addClass("epic-win");
                $(".win").fadeIn(250);

                setTimeout(resetBoard, 2500);
            }
            else {
                setTimeout(turnBoard, 1000);
            }
        }
    );

}

function turnBoard() {

    if (Math.random() < 0.25) // 25% chance that magic is going to happen.
    {
        $(".gameboard").attr("class", "gameboard gameboard-alt" + Math.ceil(Math.random() * 2));
    }
}

function resetBoard() {
    $(".card").removeClass("card-found");
    $(".gameboard").animate({opacity: 0}, 400,
        function () {
            $(".win").fadeOut(250);
            $(".card").css("opacity", 1);
            $(".gameboard").animate({opacity: 1}, 400);
        }
    );
}
